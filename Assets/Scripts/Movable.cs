﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour {
    public bool CanMove = true;
    public float Speed = 2.0f;

    private VirtualJoystick _joystick;
	// Use this for initialization
	void Start () {
        _joystick = GameObject.FindGameObjectWithTag("VirtualJoystick").GetComponent<VirtualJoystick>();
	}
	
	// Update is called once per frame
	void Update () {
        if(CanMove) {
            Vector3 tmp_moveVector = transform.forward * _joystick.Vertical() + transform.right * _joystick.Horizontal();
            iTween.MoveUpdate(gameObject, transform.position + (tmp_moveVector * Speed * Time.deltaTime), 0);
        }
    }

    public void SetMovableState(bool state)
    {
        CanMove = state;
    }
}
