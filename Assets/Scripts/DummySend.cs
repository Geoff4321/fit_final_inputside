﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniOSC;
using OSCsharp.Data;

public class DummySend : UniOSCEventDispatcher {
    [SerializeField]
    private Transform _TeacherTransform;
    public override void Awake()
    {
        base.Awake();
    }

    public override void OnEnable()
    {
        base.OnEnable();
        ClearData();


        AppendData("Teacher current position");

        AppendData((float)_TeacherTransform.position.x);
        AppendData((float)_TeacherTransform.position.z);
    }

    public override void OnDisable()
    {
        base.OnDisable();

    }
    
    public void SendIT()
    {
        if (_OSCeArg.Packet is OscMessage)
        {
            OscMessage msg = ((OscMessage)_OSCeArg.Packet);
            _updateOscMessageData(msg);
        }
        else if (_OSCeArg.Packet is OscBundle)
        {
            foreach (OscMessage msg2 in ((OscBundle)_OSCeArg.Packet).Messages) {
                _updateOscMessageData(msg2);
            }
        }

        _SendOSCMessage(_OSCeArg);
    }

    private void _updateOscMessageData(OscMessage msg)
    {
        msg.UpdateDataAt(1, (float)_TeacherTransform.position.x);
        msg.UpdateDataAt(2, (float)_TeacherTransform.position.z);
    }
}
