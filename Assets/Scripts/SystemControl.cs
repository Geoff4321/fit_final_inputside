﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemControl : MonoBehaviour {
    public Camera TeacherCamera;
    public List<Camera> StudentCameraList = new List<Camera>();
    public int CurrentStudentCameraID = 0;
    public RenderTexture CameraView;

    public bool IsStudentView = false;

    [SerializeField] private Camera _currentActivedCamera;

    // Use this for initialization
    void Start () {
        _currentActivedCamera = TeacherCamera;//StudentCameraList[CurrentCameraID];
        _currentActivedCamera.targetTexture = CameraView;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SwitchToStudentView()
    {
        _currentActivedCamera = StudentCameraList[CurrentStudentCameraID];
        _currentActivedCamera.targetTexture = CameraView;
        TeacherCamera.targetTexture = null;
        TeacherCamera.SendMessage("SetMovableState", false);
        IsStudentView = true;
    }

    void SwitchToSTeacherView()
    {
        _currentActivedCamera = TeacherCamera;
        _currentActivedCamera.targetTexture = CameraView;
        StudentCameraList[CurrentStudentCameraID].targetTexture = null;
        TeacherCamera.SendMessage("SetMovableState", true);
        IsStudentView = false;
    }

    
}
