﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
    private Image _holderImage;
    private Image _controlImage;
    private Shadow _controlShadow;
    private Vector3 _inputVector;

    // Use this for initialization
    void Start () {
        _holderImage = GetComponent<Image>();
        _controlImage = transform.GetChild(0).GetComponent<Image>();

        _controlShadow = transform.GetChild(0).GetComponent<Shadow>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 tmp_currentPos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(_holderImage.rectTransform, 
                                                                   eventData.position, 
                                                                   eventData.pressEventCamera,
                                                                   out tmp_currentPos))
        {
            tmp_currentPos.x /= _holderImage.rectTransform.sizeDelta.x;
            tmp_currentPos.y /= _holderImage.rectTransform.sizeDelta.y;

            _inputVector = new Vector3(tmp_currentPos.x * 2, 0, tmp_currentPos.y * 2);
            _inputVector = (_inputVector.magnitude > 1.0f) ? _inputVector.normalized : _inputVector;

            Debug.Log(_inputVector);

            _controlImage.rectTransform.anchoredPosition = new Vector3(
                 _inputVector.x * (_holderImage.rectTransform.sizeDelta.x / 3),
                 _inputVector.z * (_holderImage.rectTransform.sizeDelta.y / 3));

			Debug.Log (_controlImage.rectTransform.anchoredPosition);
            _controlShadow.effectDistance = new Vector2(_inputVector.x, _inputVector.z) * -8.0f;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _inputVector = Vector3.zero;
        _controlImage.rectTransform.anchoredPosition = _inputVector;
        _controlShadow.effectDistance = _inputVector;
    }

    public float Horizontal()
    {
        if(_inputVector.x != 0)
        {
            return _inputVector.x;
        }
        else
        {
            return Input.GetAxis("Horizontal");
        }
    }

    public float Vertical()
    {
        if (_inputVector.z != 0)
        {
            return _inputVector.z;
        }
        else
        {
            return Input.GetAxis("Vertical");
        }
    }
}
