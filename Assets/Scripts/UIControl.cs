﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour {

    [SerializeField] Toggle ViewSwtichToggle;

	// Use this for initialization
	void Start () {
        ViewSwtichToggle.onValueChanged.AddListener(delegate { SwitchView(); });

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SwitchView()
    {
        if (ViewSwtichToggle.isOn)
        {
            SendMessageUpwards("SwitchToStudentView");
        }
        else
        {
            SendMessageUpwards("SwitchToSTeacherView");
        }
    }
}
