﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniOSC;
using OSCsharp.Data;
using System;

public class DummyReciever : UniOSCEventTarget {
    public Transform StudentTransform;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void OnOSCMessageReceived(UniOSCEventArgs args)
    {
        OscMessage msg = (OscMessage)args.Packet;
        if (msg.Data.Count < 1) return;

        Debug.Log(msg.Data[0]);

        /*
        iTween.MoveTo(StudentTransform.gameObject,
            new Vector3((float)msg.Data[1], StudentTransform.position.y, (float)msg.Data[2]),
            1.0f);
        */

        StudentTransform.position = new Vector3((float)msg.Data[1], StudentTransform.position.y, (float)msg.Data[2]);

        Debug.Log(String.Format("{0} : {1}", msg.Data[1], msg.Data[2]));

        //transform.position = new Vector3(msg.Data[0], msg.Data[1], msg.Data[2]);
    }
}
